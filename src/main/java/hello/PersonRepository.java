package hello;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by jong on 11/10/15.
 */
@Repository
public interface PersonRepository extends CrudRepository<Person, Integer>{

    public Person findByName(String name);
}
