package hello;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by jong on 11/10/15.
 */
@RestController
public class FamilyController {

    @Autowired
    private FamilyRepository repository;

    @Autowired
    private PersonRepository personRepository;

    @RequestMapping("/families")
    public Iterable<Family> getFamilies() {
        return repository.findAll();
    }

    // curl -H "Content-Type: application/json" -X POST -d '{}' http://localhost:8080/family/create
    @RequestMapping("/family/create")
    public Family createNewFamily(@RequestBody Family family) {
        return repository.save(family);
    }

    // curl -H "Content-Type: application/json" -X POST -d '{}' http://localhost:8080/family/add/1?personId=1
    @RequestMapping(value = "/family/add/{familyId}", method = RequestMethod.POST)
    public Family addPersonToFamily(@PathVariable(value = "familyId") int familyId,
                                    @RequestParam(value = "personId") int personId) {

        Person p = personRepository.findOne(personId);
        Family f = repository.findOne(familyId);
        f.getPersons().add(p);

        return repository.save(f);
    }

    // curl -H "Content-Type: application/json" -X POST -d '{}' http://localhost:8080/family/removePerson/1?personId=2
    @RequestMapping(value = "/family/removePerson/{familyId}", method = RequestMethod.POST)
    public void removePerson(@PathVariable(value = "familyId") int familyId,
                             @RequestParam(value = "personId") int personId) {
        Family f = repository.findOne(familyId);
        List<Person> people = f.getPersons();

        Person p = personRepository.findOne(personId);
        if (people.contains(p)) {
            people.remove(p);
            repository.save(f);
        }
    }

    @RequestMapping(value = "/family/delete", method = RequestMethod.DELETE)
    public void deleteFamily(@RequestParam("id") int id) {
        repository.delete(id);
    }
}
