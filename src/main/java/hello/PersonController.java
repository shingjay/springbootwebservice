package hello;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by jong on 11/10/15.
 */
@RestController
public class PersonController {

    @Autowired
    private PersonRepository repository;

    //http://localhost:8080/person/2
    @RequestMapping(value = "/person/{id}", method = RequestMethod.GET)
    public Person findById(@PathVariable("id") int id) {
        return repository.findOne(id);
    }

    //http://localhost:8080/personByName?name=jay
    @RequestMapping(value = "/personByName")
    public Person findByName(@RequestParam("name") String name) {
        return repository.findByName(name);
    }

    @RequestMapping(value = "/persons", method = RequestMethod.GET)
    public Iterable<Person> findAll() {
        return repository.findAll();
    }

    @RequestMapping(value = "/person", method = RequestMethod.POST)
    public Person createPerson(@RequestBody Person person) {
        return repository.save(person);
    }

    //curl -H "Content-Type: application/json" -X DELETE http://localhost:8080/person/delete?id=1
    @RequestMapping(value = "/person/delete", method = RequestMethod.DELETE)
    public void deletePersonById(@RequestParam("id") int id) {
        repository.delete(id);
    }

    // curl -H "Content-Type: application/json" -X PUT -d '{"name":"jack"}' http://localhost:8080/person/update?id=2
    @RequestMapping(value = "/person/update", method = RequestMethod.PUT)
    public Person updatePerson(@RequestParam("id") int id, @RequestBody Person person) {
        Person p = repository.findOne(id);
        p.setName(person.getName());
        return repository.save(p);
    }

}
